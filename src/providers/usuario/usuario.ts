import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {AlertController, Platform, ModalController} from "ionic-angular";
import { PrincipalPage, LoginPage }  from "../../pages/index.paginas";
import { URL_SERVICIOS } from "../../config/url.servicios";
import { Storage }  from "@ionic/storage";
import { empty } from 'rxjs/Observer';
import { WebServiceProvider } from "../web-service/web-service";


/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

id:string;
perfil:any[] = [];
mediciones:any[]=[];

  constructor(public http:Http,
              private alertCtrl:AlertController,
              private platform: Platform,
              private stor: Storage,
              private servicio: WebServiceProvider,
              private modalCtrl: ModalController) {

              if(localStorage.getItem("id"))
              {
                console.log("alert");
                this.id=localStorage.getItem("id");
              }else
              {
                this.id=this.servicio.id;
              }
  }

  dataUpdate(nombre:string,apellidoP:string,apellidoM:string,fechaN:string,numeroT:string)
  {
    console.log(this.id);
    let data= new URLSearchParams();
    data.append("nombre", nombre);
    data.append("apellidoP",apellidoP);
    data.append("apellidoM", apellidoM);
    data.append("fechaN", fechaN);
    data.append("numeroT", numeroT);
    data.append("id",this.id);
   let url= URL_SERVICIOS+"actualizar";

    return this.http.post( url, data )
            .map(resp=>{
              let data_resp = resp.json();
              console.log( data_resp );
              console.log("hola");
              if( data_resp.error )
              {
                this.alertCtrl.create({
                  title:"Error al Actualizar",
                  subTitle: data_resp.mensaje,
                  buttons: ["OK"]
                }).present();
              }else{

                this.alertCtrl.create({
                  title:"Actualizado",
                  subTitle: data_resp.mensaje,
                  buttons: ["OK"]
                }).present();
               
              }

            });
  }
  cargar()
  {
    console.log(this.id);
    let url = URL_SERVICIOS + "usuario/"+this.id;
    this.http.get(url)
    .map( resp => resp.json())
    .subscribe(data=>{
      console.log(data);
      if(data.error)
      {
        console.log(data.mensaje);
      }else
      {
        this.perfil.push(...data.usuario); 
      }
    })

  }

  cargarM()
  {
    console.log(this.id);
    let url = URL_SERVICIOS + "mediciones/"+this.id;
    this.http.get(url)
    .map( resp => resp.json())
    .subscribe(data=>{
      console.log(data);
      if(data.error)
      {
        console.log(data.mensaje);
      }else
      {
        this.mediciones.push(...data.medicion); 
      }
    })

  }

}
