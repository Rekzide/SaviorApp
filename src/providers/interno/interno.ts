import { Injectable } from '@angular/core';
import { ScannData } from "../../models/scan-data.models";
import { InAppBrowser } from "@ionic-native/in-app-browser";
@Injectable()
export class InternoProvider {

  private _historial:ScannData[]=[];

  constructor(private inAppBrowser: InAppBrowser) {
   
  }

  abrir_scan(index:number)
  {
    let scanData = this._historial[index];
    console.log(scanData);

    switch (scanData.tipo) {
      case "http":
          this.inAppBrowser.create(scanData.info,"_system");
      break;
      default:
        console.log("Tipo no soportado");
    }
  }

  agregar_historial(texto:string)
  {
    let data = new ScannData(texto);

    this._historial.unshift(data);
    console.log(this._historial);

    this.abrir_scan(0);
  }

  cargar_historial()
  {
    return this._historial;
  }




}
