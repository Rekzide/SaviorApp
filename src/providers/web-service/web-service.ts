import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { URL_SERVICIOS }  from "../../config/url.servicios";
import {AlertController, Platform} from "ionic-angular";


import { Storage }  from "@ionic/storage";

@Injectable()
export class WebServiceProvider {

  token:string;
  token2:string;
  idR:string;
  id:string;
  correo:string;
  usuario:string;
  clave:string;
  pUsuario:any[] = [];
  pUsuario2:any[] = [];
  //casa bryan 
  //api:string='http://192.168.1.20/RestPhp/index.php/';
  //Servidor Local
  api:string='https://www.saviortech.tk/SaviorRest/index.php/';  

  constructor(public http:Http,
              private alertCtrl:AlertController,
              private platform: Platform,
              private stor: Storage)
  {
   
    this.cargar_storage();
  }

  activo():boolean{ 
    if(this.token )
    {
      return true;
    }else
    {
      return false;
    }
  }

  recuperar(correo:string)
  {
    let data=new URLSearchParams();
    data.append("correo", correo);
    let url = URL_SERVICIOS+"recuperacion";
    return this.http.post(url, data)
            .map(resp=>{
              let data_resp = resp.json();
              if(data_resp.error)
              {
                this.alertCtrl.create({
                  title: "Correo Invalido",
                  subTitle: data_resp.mensaje,
                  buttons: ["Ok"]
                }).present();

              }else
              {
                console.log("hola");
                this.alertCtrl.create({
                  title: "¡Exito!",
                  subTitle: data_resp.mensaje,
                  buttons:[{
                    text:"Ok",
                    handler: data =>
                    {
                      window.location.reload();
                    }
                  }]
                }).present();
              }
            });
  }
  

  ingresar(correo:string, clave:string)
  { 
    console.log("hola3");
    let data= new URLSearchParams();
    data.append("correo", correo);
    data.append("clave",clave);
    let url= URL_SERVICIOS+"login";
    console.log(url);

    return this.http.post( url, data )
            .map(resp=>{
              let data_resp = resp.json();
              console.log( data_resp );
              console.log("hola");
              if( data_resp.error )
              {
                this.alertCtrl.create({
                  title:"Error al iniciar",
                  subTitle: data_resp.mensaje,
                  buttons: ["OK"]
                }).present();
              }else{
                this.token = data_resp.token;
                this.id = data_resp.id;
                this.usuario = data_resp.nombre_usuario;
                this.correo = data_resp.correo;
                this.clave = data_resp.clave;
                this.pUsuario.push(...data_resp.perfil);
                console.log("hoal222");
                //guardar storage
                this.guardar_storage();
              }

            });

  }

  cerrar_sesion()
  {
    this.token=null;
    this.id= null;
    this.usuario=null;
    this.correo=null;
    this.clave = null;
    this.pUsuario= null;
    

    //guardar storage
    this.guardar_storage();
  }

private guardar_storage()
{
  if(this.platform.is("android"))
  {
    this.stor.set('token',this.token);
    this.stor.set('id', this.id);
    this.stor.set('usuario',this.usuario);
    this.stor.set('correo',this.correo);
    this.stor.set('clave',this.clave);
    this.stor.set('perfil', JSON.stringify(this.pUsuario));
  }
  else
  {
    //computadora
    if(this.token){
    localStorage.setItem("token", this.token );
    localStorage.setItem("id", this.id );
    localStorage.setItem("usuario", this.usuario );
    localStorage.setItem("correo", this.correo );
    localStorage.setItem("clave", this.clave );
    localStorage.setItem("perfil", JSON.stringify(this.pUsuario));

  }else{
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("usuario");
    localStorage.removeItem("correo");
    localStorage.removeItem("clave");
    localStorage.removeItem("perfil");
  }
  }
}

cargar_storage()
{
    let promesa = new Promise( ( resolve, reject )=>{
      if(this.platform.is("android"))
      {
        //dispositivo
        this.stor.ready().then( ()=>{
          this.stor.get("token").then(token=>{
            if(token)
            {
              this.token =token;
            }
          })
         this.stor.get("id").then(id=>{
            if(id)
            {
              this.id =id;
            }
            resolve();
          })
          this.stor.get("usuario").then(usuario=>{
            if(usuario)
            {
              this.usuario =usuario;
            }
            resolve();
          })
          this.stor.get("correo").then(correo=>{
            if(correo)
            {
              this.correo =this.correo;
            }
            resolve();
          })
          this.stor.get("clave").then(clave=>{
            if(clave)
            {
              this.clave =this.clave;
            }
            resolve();
          })
          this.stor.get("perfil").then(pUsuario=>{
            if(this.pUsuario)
            {
              this.pUsuario =this.pUsuario;
            }
            resolve();
          })


      })
    }
    else
    {
      //computadora
      if(localStorage.getItem("token"))
      {
        //existe items en el localstorage
        this.token = localStorage.getItem("token");
        this.id= localStorage.getItem("id");
        this.usuario= localStorage.getItem("usuario");
        this.correo= localStorage.getItem("correo");
        this.clave= localStorage.getItem("clave");
        this.pUsuario = JSON.parse(localStorage.getItem("perfil"));
      }
    }
    })
}

  getData()
  {
    return this.http.get(this.api+'listado.php').map((res: Response)=>res.json())
  }

  dataRegistrer(parans)
  {
    let headers = new Headers ({'conten-type':'application/x-form-urlencode'});
    return this.http.post(this.api+"Singup",parans,
        {
          headers: headers,
          method: "POST"
        }).map(
          (res:Response)=> {return res.json();}
        );
  }

}
