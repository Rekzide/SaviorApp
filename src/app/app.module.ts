import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule }  from "@angular/common/http";
import { HttpModule }  from "@angular/http";
import { IonicStorageModule } from "@ionic/storage";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { NgxQRCodeModule } from "ngx-qrcode2";

import {
  PrincipalPage,
  LoginPage,
  SingupPage,
  AboutPage,
  ContactPage,
  TabsPage,
  RecuperacionPage,
  InfoUsuarioPage,
  BluetoPage
}  from "../pages/index.paginas";
import { WebServiceProvider } from '../providers/web-service/web-service';
import { InternoProvider } from '../providers/interno/interno';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { BluetoothSerial } from "@ionic-native/bluetooth-serial";
@NgModule({
  declarations: [
    MyApp,
    PrincipalPage,
    LoginPage,
    SingupPage,
    AboutPage,
    ContactPage,
    TabsPage,
    BluetoPage,
    RecuperacionPage,
    InfoUsuarioPage
  ],
  imports: [
    BrowserModule,
    NgxQRCodeModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PrincipalPage,
    LoginPage,
    SingupPage,
    AboutPage,
    ContactPage,
    TabsPage,
    BluetoPage,
    RecuperacionPage,
    InfoUsuarioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WebServiceProvider,
    InternoProvider,
    UsuarioProvider,
    BluetoothSerial,
    InAppBrowser,
    BarcodeScanner
  ]
})
export class AppModule {}
