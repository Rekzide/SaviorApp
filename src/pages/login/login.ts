import { Component } from '@angular/core';
import { Platform ,IonicPage, NavController, NavParams, MenuController, ViewController, ModalController } from 'ionic-angular';
import { SingupPage, PrincipalPage, TabsPage, RecuperacionPage } from '../index.paginas';
import { WebServiceProvider } from '../../providers/web-service/web-service';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  correo:string="";
  clave:string="";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private menuCtrl: MenuController,
              private viewCtrl: ViewController,
              private webser:WebServiceProvider,
              private platform:Platform,
              private modalCtrl: ModalController)
              {
                this.cargar();
              }
            

    cargar()
    {
      if(this.webser.activo()==true)
      {
        this.navCtrl.setRoot(TabsPage);
      }
    }

  ingresar()
  {
    this.webser.ingresar(this.correo, this.clave).subscribe(()=>
    {
      if( this.webser.activo()==true)
      {
        this.navCtrl.setRoot(TabsPage);
      }
    })
  }

  registro()
  {
    this.navCtrl.push(SingupPage);
  }

  recuperar()
  {
    this.navCtrl.push(RecuperacionPage);
  }

}
