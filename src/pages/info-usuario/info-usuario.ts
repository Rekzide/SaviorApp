import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {WebServiceProvider} from "../../providers/web-service/web-service";
import {UsuarioProvider} from "../../providers/usuario/usuario";
import {LoginPage, PrincipalPage} from "../../pages/index.paginas";

/**
 * Generated class for the InfoUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-info-usuario',
  templateUrl: 'info-usuario.html',
})
export class InfoUsuarioPage {
  nombre:string;
  apellidoP:string;
  apellidoM:string;
  fechaN:string;
  numeroT:string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public webservice: WebServiceProvider, 
              public alertCtrl: AlertController, 
              public uService: UsuarioProvider) 
{

}

doRefresh(refresher) {
  console.log('Begin async operation', refresher);

  setTimeout(() => {
    console.log('Async operation has ended');
    window.location.reload();
    refresher.complete();
  }, 2000);
}

  actualizarDato()
    {
      console.log(this.numeroT);
      this.uService.dataUpdate(this.nombre,this.apellidoP,this.apellidoM,this.fechaN,this.numeroT).subscribe(()=>
      {

      })
    }


showAlert(men)
{
   let alert = this.alertCtrl.create({
      title: 'Informacion',
      subTitle: men,
      buttons: ['OK']
   });
   alert.present();
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoUsuarioPage');
  }

  cerrar_sesion()
  {
    this.webservice.cerrar_sesion();
    window.location.reload();
      this.navCtrl.push(LoginPage);
  
  }


}


