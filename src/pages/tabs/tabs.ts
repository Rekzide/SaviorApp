import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ViewController, ModalController } from 'ionic-angular';
import { PrincipalPage, AboutPage, ContactPage, InfoUsuarioPage,BluetoPage }  from "../index.paginas";
import { WebServiceProvider } from '../../providers/web-service/web-service';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PrincipalPage;
  tab2Root = InfoUsuarioPage ;
  tab3Root = ContactPage;
  tab4Root = AboutPage;
  tab5Root = BluetoPage;

  constructor(private servicio: WebServiceProvider, public navCtrl: NavController ) {



  }

}
