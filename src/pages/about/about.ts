import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WebServiceProvider } from "../../providers/web-service/web-service";
import {LoginPage} from "../../pages/index.paginas";
import { InternoProvider } from "../../providers/interno/interno";
import { ScannData } from '../../models/scan-data.models';
import { UsuarioProvider } from "../../providers/usuario/usuario";



@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  historial: ScannData[]=[];

  constructor(public navCtrl: NavController,
              private userService: UsuarioProvider,
              private webservice: WebServiceProvider,
              private _historialService: InternoProvider) 
  {
      this.userService.cargarM();
  }

  ionViewDidLoad()
  {
    this.historial = this._historialService.cargar_historial();
  }




  cerrar_sesion()
  {
    this.webservice.cerrar_sesion();
    window.location.reload();
      this.navCtrl.push(LoginPage);
  
  }

}
