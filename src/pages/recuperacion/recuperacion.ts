import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WebServiceProvider } from "../../providers/web-service/web-service";

@Component({
  selector: 'page-recuperacion',
  templateUrl: 'recuperacion.html',
})
export class RecuperacionPage {

  correo:string="";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private service:WebServiceProvider) 
  
  {

  }


  validar()
  {
    this.service.recuperar(this.correo).subscribe(()=>
    {
     
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecuperacionPage');
  }

}
