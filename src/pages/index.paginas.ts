export { SingupPage } from "./singup/singup";
export { TabsPage } from "./tabs/tabs";
export { PrincipalPage } from "./principal/principal";
export { AboutPage } from "./about/about";
export { ContactPage } from "./contact/contact";
export {LoginPage } from "./login/login";
export {InfoUsuarioPage} from "./info-usuario/info-usuario";
export {RecuperacionPage} from './recuperacion/recuperacion';
export { BluetoPage } from './blueto/blueto'; 
