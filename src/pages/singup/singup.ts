import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { WebServiceProvider } from '../../providers/web-service/web-service';
import { LoginPage } from "../index.paginas";

@Component({
  selector: 'page-singup',
  templateUrl: 'singup.html',
})
export class SingupPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public service: WebServiceProvider, 
              public alertCtrl: AlertController) 
  {

  }


    envioDato( req )
    {
      console.log(req.value);
        this.service.dataRegistrer(req.value)
        .subscribe(
          data=> {
                  if(!data.error)
                  {
                    console.log(data.error);
                    this.navCtrl.setRoot(LoginPage);
                  }
                  this.showAlert(data.mensaje);
                  console.log(data.mensaje+data.error)},
        err=>console.log(err)
      );
    }

  showAlert(men)
  {
     let alert = this.alertCtrl.create({
        title: 'Informacion',
        subTitle: men,
        buttons: ['OK']
     });
     alert.present();
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad SingupPage');
  }



}
