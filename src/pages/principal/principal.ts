import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ViewController, ModalController, Platform } from 'ionic-angular';
import {WebServiceProvider}  from "../../providers/web-service/web-service";
import {UsuarioProvider} from "../../providers/usuario/usuario";
import { LoginPage,TabsPage } from "../index.paginas";

import { Storage }  from "@ionic/storage";

/**
 * Generated class for the PrincipalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-principal',
  templateUrl: 'principal.html',
})
export class PrincipalPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private servicio: WebServiceProvider,
              private platform: Platform,
              private stor: Storage, 
              private userService: UsuarioProvider) 
              { 
                this.userService.cargar();
              }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      window.location.reload();
      refresher.complete();
    }, 2000);
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrincipalPage');

  }

  cerrar_sesion()
  {
    this.servicio.cerrar_sesion();
    window.location.reload();
      this.navCtrl.push(LoginPage);

  }

  
}
