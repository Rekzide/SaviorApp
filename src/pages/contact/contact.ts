import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {WebServiceProvider} from "../../providers/web-service/web-service";
import {LoginPage} from "../../pages/index.paginas";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { ToastController, Platform } from "ionic-angular";
import { InternoProvider } from "../../providers/interno/interno";

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  createdCode= null;
  id=null;sss

  constructor(public navCtrl: NavController,
              private webservice: WebServiceProvider,
              private barcodeScanner: BarcodeScanner,
              private toasCtrl: ToastController,
              private platform: Platform,
              private _historialservice: InternoProvider) {

  }

  createScan()
  {
    this.id=this.webservice.id;
    this.createdCode= "https://saviorrate.tk?uid="+this.id;
  }

  
  scan()
  {
    console.log("Realizando Scanner..");

    if(!this.platform.is('cordova')){
      this._historialservice.agregar_historial("http://google.com");
      return;
    }

    this.barcodeScanner.scan().then((barcodeData)=>{
      console.log("result:",barcodeData.text);
      console.log("format:",barcodeData.format);
      console.log("cancelled:", barcodeData.cancelled);

      if(barcodeData.cancelled == false && barcodeData.text != null)
      {
        this._historialservice.agregar_historial(barcodeData.text);
      }

    }, (err) =>{
      console.log("Error: ",err);
      this.mostrar_error("Error: "+ err);

    });
  }

  mostrar_error(mensaje:string)
  {
    let toast = this.toasCtrl.create({
      message: mensaje,
      duration: 2000
    });

    toast.present();
  }

  cerrar_sesion()
  {
    this.webservice.cerrar_sesion();
    window.location.reload();
      this.navCtrl.push(LoginPage);
  
  }

}
