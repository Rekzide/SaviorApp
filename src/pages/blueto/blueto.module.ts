import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BluetoPage } from './blueto';

@NgModule({
  declarations: [
    BluetoPage,
  ],
  imports: [
    IonicPageModule.forChild(BluetoPage),
  ],
})
export class BluetoPageModule {}
